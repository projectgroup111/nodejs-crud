//khai bao thu vien express
const express = require("express")

// Import user controller
const userController = require("../controllers/useController");

//Tạo ra router
const userRouter = express.Router();

userRouter.post('/users', userController.createUser);
userRouter.get('/users', userController.getAllUser);
userRouter.get('/users/:id', userController.getUserById);
userRouter.put('/users/:id', userController.updateUserById);
userRouter.delete('/users/:id', userController.deleteUserById);

module.exports = { userRouter }